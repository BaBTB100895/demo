package util;

import com.fpt.ivs.at.core.object.TestDataObject;

public class TestDataObjectHandler {
    public TestDataObject setTestDataObject(TestDataObject table) {
        return table;
    }

    public TestDataObject testDataObjectAddRow(TestDataObject table, String value) {
        if(table == null || table.getRowCount() == 0) {
            table = new TestDataObject(new String[]{"Text"}, new String[0][0]);
        }
        String[] headers = table.getColumns();
        String[][] datas = new String[table.getRowCount() + 1][headers.length];

        for(int idxRow = 0; idxRow < table.getRowCount(); idxRow++) {
            String[] row = new String[headers.length];
            for(int idxCol = 0; idxCol < headers.length; idxCol++) {
                row[idxCol] = table.getCellValue(headers[idxCol], idxRow);
            }
            datas[idxRow] = row;
        }
        String[] row = new String[headers.length];
        for(int idxCol = 0; idxCol < headers.length; idxCol++) {
            row[idxCol] = value;
        }
        datas[datas.length  -1] = row;
        return new TestDataObject(headers, datas);
    }
}