package util;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.fpt.ivs.at.core.object.TestDataObject;
import com.fpt.ivs.at.core.utilities.WebDriverUtilities;

public class Math {
    public Integer maxInt(Integer a, Integer b) {
        return java.lang.Math.max(a, b);
    }
}