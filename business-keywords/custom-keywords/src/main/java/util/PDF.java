package util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;


import javax.imageio.ImageIO;

import com.fpt.ivs.at.core.object.TestDataObject;
import com.fpt.ivs.at.core.utilities.ReportUtilities;
//import com.vantuyen361.pdf.PDFEntity;
//import com.vantuyen361.pdf.PDFLine;
//import com.vantuyen361.pdf.PDFTable;
//import com.vantuyen361.pdf.PDFTableEngine;

import junit.framework.Assert;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.openqa.selenium.WebDriver;

import java.awt.image.BufferedImage;
import org.apache.pdfbox.*;

public class PDF {

    public Integer getNumberPage(String path) throws IOException {
        int number = 0;
        try
        {
        File file = Paths.get(path).toFile();
        PDDocument doc = PDDocument.load(file);
        number = doc.getNumberOfPages();
        }
        catch (IOException e)
    {
        e.printStackTrace();
    }
    return number;
}
public void verifyContainsTextInTextPDF(String textToVerify, String pathString) throws IOException
{
    try {
        String pdfOutput = null;
        File file = Paths.get(pathString).toFile();
        PDDocument document = PDDocument.load(file);
        pdfOutput = new PDFTextStripper().getText(document);
        Assert.assertEquals(textToVerify,pdfOutput);
    }
    catch (IOException e)
    {
        e.printStackTrace();
    }
}
public String getTextPage(String path) throws IOException
    {

    PDDocument document = null;
    String pdfText = null;

    try
    {
        document = PDDocument.load( new File(path));
        PDFTextStripper stripper = new PDFTextStripper();
        pdfText = stripper.getText(document).toString();

    }
    catch (IOException e)
    {
        e.printStackTrace();
    }
    finally
    {
        if( document != null )
        {
            document.close();
        }
    }
    return pdfText;
}
   /* private List<PDFEntity> getPDFOfPageObjects(String path, Integer pageNumber) throws IOException {
        File file = Paths.get(path).toFile();
        PDDocument doc = PDDocument.load(file);
        PDFTableEngine engine = new PDFTableEngine();
        engine.setPage(pageNumber);
        engine.scan(doc);
        doc.close();
        return engine.getEntities();
    }

    public Integer getNumberOfPageObject(String path, Integer pageNumber) throws IOException {
        return getPDFOfPageObjects(path, pageNumber).size();
    }

    public String getPDFObjectType(String path, Integer pageNumber, Integer objIdx) throws IOException {
        objIdx = objIdx - 1;
        PDFEntity entity = getPDFOfPageObjects(path, pageNumber).get(objIdx);
        return entity.getClass().getSimpleName();
    }

    public String getPDFObjectAsString(String path, Integer pageNumber, Integer objIdx) throws IOException {
        objIdx = objIdx - 1;
        PDFEntity entity = getPDFOfPageObjects(path, pageNumber).get(objIdx);
        return entity.toString();
    }

    public TestDataObject getPDFObjectAsTestDataObject(String path, Integer pageNumber, Integer objIdx) throws IOException {
        objIdx = objIdx - 1;
        PDFEntity entity = getPDFOfPageObjects(path, pageNumber).get(objIdx);
        if(!(entity instanceof PDFTable)) throw new IOException("Object isn't PDFTable");
        PDFTable table = (PDFTable)entity;
        // Build table header
        String [] columns = new String[table.getNumberColumn()];
        for(int idx = 0; idx < columns.length; idx++) {
            columns[idx] = "Column_" + (idx + 1);
        }
        // Build table data
        String [][] dataTable = new String[table.getNumberRow()][table.getNumberColumn()];
        for(int idxRow = 0; idxRow < dataTable.length; idxRow++) {
            String [] row = dataTable[idxRow];
            for(int idxCell = 0; idxCell < row.length; idxCell++) {
                row[idxCell] = table.getCellString(idxRow, idxCell);
            }
        }
        return new TestDataObject(columns, dataTable);
    }*/

    public String pageToImage(String path, Integer pageNumber) throws IOException {
        File file = Paths.get(path).toFile();
        PDDocument document = PDDocument.load(file);
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        BufferedImage bim = pdfRenderer.renderImageWithDPI(pageNumber, 300, ImageType.RGB);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bim, "png", baos);
        Path imageReportPath = ReportUtilities.saveImageForReport(baos.toByteArray());
        document.close();
        return imageReportPath.toString(); 
    }
}