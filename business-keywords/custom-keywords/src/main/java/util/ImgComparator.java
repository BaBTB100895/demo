package util;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Point;

public class ImgComparator {
    public static enum Axis {
        VERTICAL,
        HORIZONTAL
    }
    protected double allowPctDiffNumber = 0;

    public ImgComparator setAllowPctDiffNumber(double allowPctDiffNumber) {
        this.allowPctDiffNumber = allowPctDiffNumber;
        return this;
    }

    public static double getPctDiff(BufferedImage im1, BufferedImage im2) {
        int width = java.lang.Math.max(im1.getWidth(), im2.getWidth());
        int height = java.lang.Math.max(im1.getHeight(), im2.getHeight());
        int minWidth = java.lang.Math.min(im1.getWidth(), im2.getWidth());
        int minHeight = java.lang.Math.min(im1.getHeight(), im2.getHeight());
        int totalPixcel = width * height;
        int minPixcel = minWidth * minHeight;
        return (totalPixcel - minPixcel) / (double)totalPixcel;
    }

    protected Integer[][] compareImgs(BufferedImage im1, BufferedImage im2) {
        int width = im1.getWidth() < im2.getWidth() ? im1.getWidth() : im2.getWidth();
        int height = im1.getHeight() < im2.getHeight() ? im1.getHeight() : im2.getHeight();
        // Integer[][] result = (Integer[][]) new Integer[java.lang.Math.max(im1.getHeight(), im2.getHeight())][java.lang.Math.max(im1.getWidth(), im2.getWidth())];
        Integer[][] result = (Integer[][]) new Integer[java.lang.Math.min(im1.getHeight(), im2.getHeight())][java.lang.Math.min(im1.getWidth(), im2.getWidth())];
        result = initDataTable(result, Integer.valueOf(1));
        for(int idxHeight = 0; idxHeight < height; idxHeight ++) {
            for(int idxWidth = 0; idxWidth < width; idxWidth ++) {
                // RGB color1 = RGB.convertToRGB(im1.getRGB(idxWidth,idxHeight));
                RGB color1 = RGB.convertToRGB(im1, idxWidth, idxHeight);
                // RGB color2 = RGB.convertToRGB(im2.getRGB(idxWidth,idxHeight));
                RGB color2 = RGB.convertToRGB(im2, idxWidth, idxHeight);
                double pct = color1.pctDiff(color2);
                if(allowPctDiffNumber >= pct) {
                    result[idxHeight][idxWidth] = Integer.valueOf(0);
                }
            }
        }
        return result;
    }

    public BufferedImage highLightDiff(BufferedImage img, Integer[][] comparedPixcels) {
        int matrixHeight = comparedPixcels.length;
        int matrixWidth = matrixHeight == 0 ? matrixHeight : comparedPixcels[0].length;
        BufferedImage outputImg = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
        for(int idxHeight = 0; idxHeight < img.getHeight(); idxHeight ++) {
            for(int idxWidth = 0; idxWidth < img.getWidth(); idxWidth ++) {
                int intColor = img.getRGB(idxWidth, idxHeight);
                if(idxHeight >= matrixHeight || idxWidth >= matrixWidth || comparedPixcels[idxHeight][idxWidth].equals(1)) {
                    RGB color = RGB.convertToRGB(intColor);
                    outputImg.setRGB(idxWidth, idxHeight, new Color(200, (int)color.getGreen()/2, (int)color.getBlue()/2).getRGB());
                } else {
                    outputImg.setRGB(idxWidth, idxHeight, intColor);
                }
            }
        }
        return outputImg;
    }

    public BufferedImage mergePicture(BufferedImage img1, BufferedImage img2, Axis axis) {
        int width = axis == Axis.HORIZONTAL? img1.getWidth() + img2.getWidth() : java.lang.Math.max(img1.getWidth(), img2.getWidth());
        int height = axis == Axis.HORIZONTAL? java.lang.Math.max(img1.getHeight(), img2.getHeight()) : img1.getHeight() + img2.getHeight();
        BufferedImage outputImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for(int idxWidth = 0; idxWidth < width; idxWidth ++) {
            for(int idxHeight = 0; idxHeight < height; idxHeight ++) {
                if(idxWidth >= img1.getWidth()) {
                    if(idxWidth - img1.getWidth() >= img2.getWidth() || idxHeight >= img2.getHeight()) continue;
                    outputImg.setRGB(idxWidth, idxHeight, img2.getRGB(idxWidth - img1.getWidth(), idxHeight));
                } else {
                    if(idxWidth >= img1.getWidth() || idxHeight >= img1.getHeight()) continue;
                    outputImg.setRGB(idxWidth, idxHeight, img1.getRGB(idxWidth, idxHeight));
                }
            }
        }
        return outputImg;
    }

    public double getPercentDiff(Integer[][] comparedPixcels) {
        if(comparedPixcels.length == 0 || comparedPixcels[0].length == 0) return 100;
        int numDiff = 0;
        int numCol = 0;
        for(int i = 0; i < comparedPixcels.length; i++) {
            numCol = java.lang.Math.max(numCol, comparedPixcels[i].length);
            for(int j = 0; j < comparedPixcels[i].length; j++) {
                if(comparedPixcels[i][j] <= 0) continue;
                numDiff += 1;
            }
        }
        return (double)numDiff/(comparedPixcels.length * numCol);
    }

    public List<Rectangle> getDiffElemBox(Integer[][] comparedPixcels) {
        List<List<Point>> elems = findAdjacentPoints(comparedPixcels);
        // System.out.println("mapElemIdxs: " + new Gson().toJson(elems));
        List<Rectangle> elemboxs = new ArrayList<>();
        // get boxs of AdjacentPoints
        for(List<Point> elem : elems) {
            Rectangle elemBox = getBox(elem);
            if(elemBox == null) continue;
            // if(elemBox == null) elemBox = new Rectangle(0, 0, 10, 10);
            elemboxs.add(elemBox);
        }
        // check if there are a box contain a pixcel of other element
        int currSize = 0;
        while(currSize != elemboxs.size()) {
            currSize = elemboxs.size();
            for(int idx = 0; idx < elemboxs.size(); idx ++) {
                Rectangle elemBox = elemboxs.get(idx);
                List<Point> elem = elems.get(idx);
                for(int idxOtherElem = idx + 1; idxOtherElem < elemboxs.size(); idxOtherElem ++) {
                    List<Point> otherElem = elems.get(idxOtherElem);
                    // System.out.println("Distance: "  + distance(elemBox, elemboxs.get(idxOtherElem)));
                    if(checkPointInBox(elemBox, otherElem) 
                        || checkPointInBox(elemboxs.get(idxOtherElem), elem) 
                        || elemBox.contains(elemboxs.get(idxOtherElem)) 
                        || elemboxs.get(idxOtherElem).contains(elemBox)
                        || distance(elemBox, elemboxs.get(idxOtherElem)) <= 20 ) {
                        // this and other element is same
                        elem.addAll(otherElem);
                        elemBox = getBox(elem);
                        // remove element
                        elems.remove(idxOtherElem);
                        elemboxs.remove(idxOtherElem);
                        elemboxs.remove(idx);
                        elemboxs.add(idx, elemBox);
                        idxOtherElem -= 1;
                    }
                }
            }
        }
        return elemboxs;
    }

    public double distance(Rectangle box1, Rectangle box2) {
        if(box1.intersects(box2)) {
            return 0;
        }
        Rectangle result = new Rectangle();
        Rectangle.union(box1, box2, result);
        int height = (int)(result.getWidth() - box1.getWidth() - box2.getWidth());
        height = height < 0 ? 0 : height;
        int width = (int)(result.getHeight() - box1.getHeight() - box2.getHeight());
        width = width < 0 ? 0 : width;
        return java.lang.Math.sqrt(height * height + width * width);
    }

    private boolean checkPointInBox(Rectangle box, List<Point> points) {
        for(Point point : points) {
            if(box.contains(point.getX(), point.getY())) return true;
        }
        return false;
    }
    
    private Rectangle getBox(List<Point> points) {
        if(points == null || points.isEmpty()) return null;
        int x = Integer.MAX_VALUE, y = Integer.MAX_VALUE, endX = Integer.MIN_VALUE, endY = Integer.MIN_VALUE;
        for(Point point : points) {
            x = java.lang.Math.min(x, point.getX());
            y = java.lang.Math.min(y, point.getY());
            endX = java.lang.Math.max(endX, point.getX());
            endY = java.lang.Math.max(endY, point.getY());
        }
        return new Rectangle(x, y, endX - x, endY - y);
    }

    public List<List<Point>> findAdjacentPoints(Integer[][] comparedPixcels) {
        List<List<Point>> elements = new ArrayList<>();
        // comparedPixcels = Arrays.stream(comparedPixcels).map(r->Arrays.copyOf(r, r.length)).toArray(Integer[][]::new);
        int maxElemIndex = 1;
        List<IdxMapping> mapElemIdxs = new ArrayList<>();
        int height = comparedPixcels.length;
        if(height == 0) return elements;
        int width = comparedPixcels[0].length;
        if(width == 0) return elements;
        // Scan and mark element index for all pixcel in matrix
        for(int yIdx = 0; yIdx < comparedPixcels.length; yIdx++) {
            for(int xIdx = 0; xIdx < comparedPixcels[yIdx].length; xIdx++) {
                if(comparedPixcels[yIdx][xIdx] != 1) continue;
                List<Point> points = findDiffNearPoints(comparedPixcels, new Point(xIdx, yIdx), 2, height, width);
                int minIdx = getMinElemIndex(comparedPixcels, points);
                Set<Integer> relativeIdxs = new HashSet<>();
                if(minIdx == Integer.MAX_VALUE) {
                    maxElemIndex += 1;
                    minIdx = maxElemIndex;
                    mapElemIdxs.add(new IdxMapping().setElem1(minIdx).setElem2(minIdx));
                }
                for(Point point : points) {
                    if(comparedPixcels[point.getY()][point.getX()] != 1) relativeIdxs.add(comparedPixcels[point.getY()][point.getX()]);
                    comparedPixcels[point.getY()][point.getX()] = minIdx;
                    // System.out.println("point(" + point.getY() + "," + point.getX() + ")=" + minIdx);
                }
                if(!relativeIdxs.isEmpty()) {
                    for(Integer relativeIdx : relativeIdxs) {
                        if(relativeIdx.equals(minIdx)) continue;
                        mapElemIdxs.add(new IdxMapping().setElem1(minIdx).setElem2(relativeIdx));
                    }
                }
                comparedPixcels[yIdx][xIdx] = minIdx;
            }
        }
        // Get List  index of each of elements
        // System.out.println("mapElemIdxs: " + new Gson().toJson(mapElemIdxs));
        List<List<Integer>> elemIdxs = new ArrayList<>();
        while(!mapElemIdxs.isEmpty()) {
            IdxMapping mapElemIdx = mapElemIdxs.get(0);
            mapElemIdxs.remove(0);
            List<Integer> elemIdx = new ArrayList<>();
            elemIdx.add(mapElemIdx.elem1);
            elemIdx.add(mapElemIdx.elem2);
            elemIdxs.add(elemIdx);
            List<Point> elem = new ArrayList<>();
            elements.add(elem);
            int currNumber = -1;
            while (currNumber != elemIdx.size()) {
                currNumber = elemIdx.size();
                for(int idx = 0; idx < mapElemIdxs.size(); idx++) {
                    mapElemIdx = mapElemIdxs.get(idx);
                    if(arrContains(elemIdx, mapElemIdx.elem1)) {
                        if(!arrContains(elemIdx, mapElemIdx.elem2)) elemIdx.add(mapElemIdx.elem2);
                        mapElemIdxs.remove(idx);
                        idx -= 1;
                    }
                }
            }
        }
        // get Point of each element
        for(int yIdx = 0; yIdx < comparedPixcels.length; yIdx++) {
            for(int xIdx = 0; xIdx < comparedPixcels[yIdx].length; xIdx++) {
                if(comparedPixcels[yIdx][xIdx] == 0) continue;
                for(int idx = 0; idx < elemIdxs.size(); idx++) {
                    if(arrContains(elemIdxs.get(idx), comparedPixcels[yIdx][xIdx])) {
                        elements.get(idx).add(new Point(xIdx, yIdx));
                        break;
                    }
                }
            }
        }
        // System.out.println("elements: " + new Gson().toJson(elements));
        return elements;
    }

    public boolean arrContains(List<Integer> array, Integer elem) {
        for(Integer arrIdx : array) {
            if(arrIdx.equals(elem)) return true;
        }
        return false;
    }

    public class IdxMapping {
        public Integer elem1;
        public Integer elem2;

        public IdxMapping setElem1(Integer elem1) {
            this.elem1 = elem1;
            return this;
        }

        public IdxMapping setElem2(Integer elem2) {
            this.elem2 = elem2;
            return this;
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }
    }

    public int getMinElemIndex(Integer[][] comparedPixcels, List<Point> points) {
        int result = Integer.MAX_VALUE;
        for(Point point : points) {
            if(comparedPixcels[point.getY()][point.getX()] == 1) continue;
            result = comparedPixcels[point.getY()][point.getX()] < result ? comparedPixcels[point.getY()][point.getX()] : result;
        }
        return result;
    }

    public static List<Point> findDiffNearPoints(Integer[][] comparedPixcels, Point point, int distance, int height, int width) {
        List<Point> adjacentPoints = new ArrayList<>();
        boolean flag = false;
        for(int xIdx = 0; xIdx <= distance; xIdx++) {
            int maxY = distance - xIdx;
            for(int yIdx = 0; yIdx <= maxY; yIdx++) {
                if(xIdx == 0 && yIdx == 0) continue;
                int x = point.getX() - xIdx;
                int y = point.getY() - yIdx;
                if(x >= 0 && y >= 0 && comparedPixcels[y][x] > 0) {
                    adjacentPoints.add(new Point(x, y));
                    flag = true;
                    break;
                }
                if(flag) break;
            }
        }
        for(int xIdx = 0; xIdx <= distance; xIdx++) {
            int maxY = distance - xIdx;
            for(int yIdx = 0; yIdx <= maxY; yIdx++) {
                if(xIdx == 0 && yIdx == 0) continue;
                int x = point.getX() - xIdx;
                int y = point.getY() + yIdx;
                if(y >= height) continue;
                if(x > 0 && comparedPixcels[y][x] != 0) adjacentPoints.add(new Point(x, y));
                x = point.getX() + xIdx;
                if(x < width && comparedPixcels[y][x] > 0) adjacentPoints.add(new Point(x, y));
            }
        }
        return adjacentPoints;
    }

    public List<Point> getDiffPoints(Integer[][] comparedPixcels) {
        List<Point> diffPoints = new ArrayList<>();
        for(int i = 0; i < comparedPixcels.length; i++) {
            for(int j = 0; j < comparedPixcels[i].length; j++) {
                if(comparedPixcels[i][j] <= 0) continue;
                diffPoints.add(new Point(j, i));
            }
        }
        return diffPoints;
    }

    @SuppressWarnings("unchecked")
    private <T extends Object> T[][] initDataTable(T[][] table, T defaultValue) {
        for(int idxRow = 0; idxRow < table.length; idxRow ++) {
            for(int idxColumn = 0; idxColumn < table[idxRow].length; idxColumn ++) {
                table[idxRow][idxColumn] = defaultValue;
            }
        }
        return table;
    }

    private static class RGB {
        double red = 0;
        double green = 0;
        double blue = 0;

        public int getRed() {
            return (int)red;
        }

        public int getBlue() {
            return (int)blue;
        }

        public int getGreen() {
            return (int)green;
        }

        public double pctDiff(RGB color) {
            return (java.lang.Math.abs(red - color.red) + java.lang.Math.abs(green - color.green) + java.lang.Math.abs(blue - color.blue))/(0.255*3);
        }

        public static RGB convertToRGB(int rgb) {
            RGB color = new RGB();
            color.red =   (rgb & 0x00ff0000) >> 16;
            color.green = (rgb & 0x0000ff00) >> 8;
            color.blue =   rgb & 0x000000ff;
            return color;
        }
        
        public static RGB convertToRGB(BufferedImage img, int x, int y) {
            RGB color;
            try {
                color = new RGB();
                byte[] arr = (byte[])img.getRaster().getDataElements(x, y, null);
                color.red = (double)arr[0];
                color.green = (double)arr[1];
                color.blue = (double)arr[2];
            } catch (Exception e) {
                System.out.println("convert failed");
                color = convertToRGB(img.getRGB(x, y));
            }
            return color;
        }
    }
}