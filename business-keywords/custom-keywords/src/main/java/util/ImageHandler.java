package util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.awt.Color;

import javax.imageio.ImageIO;

import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.BasicStroke;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.font.FontRenderContext;

import com.fpt.ivs.at.core.utilities.ReportUtilities;
import com.fpt.ivs.at.core.utilities.WebDriverUtilities;
import com.google.gson.Gson;

import org.openqa.selenium.WebDriver;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import org.apache.axis.*;
import util.ImgComparator.Axis;

public class ImageHandler {
    public static void copyReport(String path) throws IOException {
        ReportUtilities.saveImageForReport(new File(path));
    }

    public static String captureFullScreen() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        takeFullPageScreenShotAsByte(WebDriverUtilities.getDriver(), baos);
        return ReportUtilities.saveImageForReport(baos.toByteArray()).toString();
    }

    private static void takeFullPageScreenShotAsByte(WebDriver webDriver, OutputStream out) throws IOException {
        Screenshot fpScreenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000))
                .takeScreenshot(webDriver);

        BufferedImage originalImage = fpScreenshot.getImage();
        ImageIO.write(originalImage, "png", out);
    }

    // public static void verifyTwoImageSample(String path1, String path2, String maxDiffrent) throws Exception {
    //     double allowPctDiffNumber = Double.valueOf(maxDiffrent);
    //     String result = compareImage(path1, path2, "0");
    //     double percentDiff = Double.valueOf(result);
    //     if(percentDiff > allowPctDiffNumber) {
    //         throw new Exception("Two images are diffrent. percentDiff = " + percentDiff);
    //     }
    // }
    // public static String compareImage(String path1, String path2, String allowPctDiff) throws IOException {
    //     double allowPctDiffNumber = Double.valueOf(allowPctDiff);
    //     BufferedImage im1 = ImageIO.read(new File(path1));
    //     BufferedImage im2 = ImageIO.read(new File(path2));
    //     ImgComparator comparator = new ImgComparator().setAllowPctDiffNumber(allowPctDiffNumber);
    //     Integer[][] matrixCompared = comparator.compareImgs(im1, im2);
    //     im1 = comparator.highLightDiff(im1, matrixCompared);
    //     im2 = comparator.highLightDiff(im2, matrixCompared);
    //     List<Rectangle> elemBoxs = comparator.getDiffElemBox(matrixCompared);
    //     for(Rectangle elemBox: elemBoxs) {
    //         drawBox(im1, elemBox, 5);
    //         drawBox(im2, elemBox, 5);
    //     }
    //     BufferedImage outputImg = comparator.mergePicture(im1, im2, Axis.HORIZONTAL);
    //     ByteArrayOutputStream baos = new ByteArrayOutputStream();
    //     ImageIO.write(outputImg, "png", baos);
    //     ReportUtilities.saveImageForReport(baos.toByteArray());
    //     baos.close();
    //     // double variation = comparator.getPercentDiff(matrixCompared);
    //     // return Double.toString(variation);
    //     return Integer.toString(elemBoxs.size());
    //     // return new Gson().toJson(matrixCompared);
    //     // return "1";
    // }

    public static String compareImgAndReturnNumElem(String path1, String path2) throws IOException {
        BufferedImage im1 = ImageIO.read(new File(path1));
        BufferedImage im2 = ImageIO.read(new File(path2));
        ImgComparator comparator = new ImgComparator().setAllowPctDiffNumber(0);
        Integer[][] matrixCompared = comparator.compareImgs(im1, im2);
        im1 = comparator.highLightDiff(im1, matrixCompared);
        im2 = comparator.highLightDiff(im2, matrixCompared);
        List<Rectangle> elemBoxs = comparator.getDiffElemBox(matrixCompared);
        for(int idx = 0; idx <elemBoxs.size(); idx ++) {
            Rectangle elemBox = elemBoxs.get(idx);
        }
        BufferedImage outputImg = comparator.mergePicture(im1, im2, Axis.HORIZONTAL);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(outputImg, "png", baos);
        ReportUtilities.saveImageForReport(baos.toByteArray());
        baos.close();
        return Integer.toString(elemBoxs.size());
    }

    public static void verifyTwoImgAreSame(String path1, String path2, Integer thresholdNumElemDiff) throws Exception {
        String pctDiffStr = compareImgAndReturnNumElem(path1, path2);
        Integer numElem = Integer.valueOf(pctDiffStr);
        if(numElem > Integer.valueOf(thresholdNumElemDiff)) {
            throw new Exception("Two images are diffrent. numElemDiff = " + numElem);
        }
    }

    public static String compareImgAndReturnPercentDiff(String path1, String path2) throws IOException {
        BufferedImage im1 = ImageIO.read(new File(path1));
        BufferedImage im2 = ImageIO.read(new File(path2));
        ImgComparator comparator = new ImgComparator().setAllowPctDiffNumber(0);
        Integer[][] matrixCompared = comparator.compareImgs(im1, im2);
        im1 = comparator.highLightDiff(im1, matrixCompared);
        im2 = comparator.highLightDiff(im2, matrixCompared);
        List<Rectangle> elemBoxs = comparator.getDiffElemBox(matrixCompared);
        for(int idx = 0; idx <elemBoxs.size(); idx ++) {
            Rectangle elemBox = elemBoxs.get(idx);
            // drawBox(im1, elemBox, 5, idx + 1);
            // drawBox(im2, elemBox, 5, idx + 1);
        }
        BufferedImage outputImg = comparator.mergePicture(im1, im2, Axis.HORIZONTAL);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(outputImg, "png", baos);
        ReportUtilities.saveImageForReport(baos.toByteArray());
        baos.close();
        double pctDiffByPixcel = comparator.getPercentDiff(matrixCompared);
        double pctDiffBySize  = ImgComparator.getPctDiff(im1, im2);
        // return Double.toString(pctDiffByPixcel);
        return Double.toString(((1 - pctDiffBySize) * pctDiffByPixcel + pctDiffBySize) * 100);
    }
    
    public static void verifyTwoImgAreSame(String path1, String path2, String thresholdPctDiff) throws Exception {
        String pctDiffStr = compareImgAndReturnPercentDiff(path1, path2);
        double pctDiff = Double.valueOf(pctDiffStr);
        if(pctDiff > Double.valueOf(thresholdPctDiff)) {
            throw new Exception("Two images are diffrent. percentDiff = " + pctDiff);
        }
    }
    
    public static void drawBox(BufferedImage img, Rectangle box, int borderSize, int idx) {
        int x,y,endX,endY, topLeftX, topLeftY;
        borderSize = borderSize - 1;
        // Draw left border
        topLeftX = x = (int)box.getX() - borderSize >= 0 ? (int)box.getX() - borderSize : 0;
        endX = x + borderSize >= img.getWidth() ? img.getWidth() : x + borderSize;
        topLeftY = y = (int)box.getY() - borderSize >= 0 ? (int)box.getY() - borderSize : 0;
        endY = (int)box.getMaxY() + borderSize < img.getHeight() ? (int)box.getMaxY() + borderSize : img.getHeight() - 1;
        fillBox(img, new Rectangle(x, y, endX - x, endY - y), new Color(254, 0, 0).getRGB());
        // Draw right border
        endX = (int)box.getMaxX() + borderSize < img.getWidth() ? (int)box.getMaxX() + borderSize : img.getWidth() - 1;
        x = endX - borderSize >=0 ? endX - borderSize : 0;
        fillBox(img, new Rectangle(x, y, endX - x, endY - y), new Color(254, 0, 0).getRGB());
        // Draw bottom border
        x = (int)box.getX() - borderSize >= 0 ? (int)box.getX() - borderSize : 0;
        endX = (int)box.getMaxX() + borderSize < img.getWidth()? (int)box.getMaxX() + borderSize : img.getWidth() - 1;
        endY = (int)box.getMaxY() + borderSize < img.getHeight() ? (int)box.getMaxY() + borderSize : img.getHeight() - 1;
        y = endY - borderSize >= 0? endY - borderSize : 0;
        fillBox(img, new Rectangle(x, y, endX - x, endY - y), new Color(254, 0, 0).getRGB());
        // Draw top border
        y = (int)box.getY() - borderSize >= 0 ? (int)box.getY() - borderSize : 0;
        endY = y + borderSize < img.getHeight() ? y + borderSize : img.getHeight();
        fillBox(img, new Rectangle(x, y, endX - x, endY - y), new Color(254, 0, 0).getRGB());

        String str = Integer.toString(idx);
        Graphics2D g2d = img.createGraphics();
        Font f = new Font("TimesRoman", Font.BOLD, 20);
        g2d.setColor(new Color(0, 102, 184));
        g2d.setFont(f);
        FontRenderContext frc = img.getGraphics().getFontMetrics().getFontRenderContext();
        Rectangle2D rect = f.getStringBounds(str, frc);
        if(rect.getWidth() >= rect.getHeight()) {
            int cycleX = (int)(topLeftX - rect.getWidth()) - 2;
            int cycleY = (int)(topLeftY - rect.getHeight() + (rect.getWidth() - rect.getHeight()) / 2) - 2;
            g2d.setColor(new Color(255, 255, 255));
            g2d.fillOval(cycleX, cycleY, (int)rect.getWidth() + 4, (int)rect.getWidth() + 4);
            g2d.setColor(new Color(0, 102, 184));
            g2d.drawString(str, (int)(topLeftX - rect.getWidth()), topLeftY);
            g2d.setStroke(new BasicStroke(5));
            g2d.drawOval(cycleX, cycleY, (int)rect.getWidth() + 4, (int)rect.getWidth() + 4);
        } else {
            int cycleX = (int)(topLeftX - rect.getWidth() - (rect.getHeight() - rect.getWidth()) / 2);
            int cycleY = (int)(topLeftY - rect.getHeight() + (rect.getHeight() - rect.getWidth()) / 2);
            g2d.setColor(new Color(255, 255, 255));
            int edge = str.length() == 1 ? (int)(rect.getHeight()) : (int)(rect.getHeight() - (rect.getWidth() - rect.getHeight()) / 2);
            g2d.fillOval(cycleX, cycleY, edge, edge);
            g2d.setColor(new Color(0, 102, 184));
            g2d.drawString(str, (int)(topLeftX - rect.getWidth()), topLeftY);
            g2d.setStroke(new BasicStroke(5));
            g2d.drawOval(cycleX, cycleY, edge, edge);
        }
        g2d.dispose();
    }

    public static void fillBox(BufferedImage img, Rectangle box, int color) {
        for(int x = (int)box.getX(); x <= box.getMaxX(); x ++) {
            for(int y = (int)box.getY(); y <= box.getMaxY(); y ++) {
                img.setRGB(x, y, new Color(254, 0, 0).getRGB());
            }
        }
    }

    public static void showImage(String path) throws IOException {
        ReportUtilities.saveImageForReport(new File(path));
    }
}