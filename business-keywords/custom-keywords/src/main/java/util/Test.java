package util;

import com.fpt.ivs.at.core.object.TestDataObject;
import com.fpt.ivs.at.core.object.UIObject;
import com.fpt.ivs.at.core.utilities.WebDriverUtilities;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.Select;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import com.fpt.ivs.at.core.keywords.webkeyword.ElementActionKeyword;
import com.fpt.ivs.at.core.keywords.webkeyword.WaitForKeyword;
import com.fpt.ivs.at.core.keywords.webkeyword.VerifyElementKeyword;
import com.fpt.ivs.at.core.keywords.webkeyword.FormActionKeyword;

public class Test {
    protected static ElementActionKeyword elemKwd = new ElementActionKeyword();
    protected static WaitForKeyword waitKwd = new WaitForKeyword();
    protected static VerifyElementKeyword verifyKwd = new VerifyElementKeyword();
    protected static FormActionKeyword formActionKwd = new FormActionKeyword();

    public static String test() {
        try {
            Keys.valueOf("ds");
        } catch (IllegalArgumentException e){
            Keys[] keys = Keys.values();
            StringBuilder str = new StringBuilder("");
            for(Keys key: keys) {
                str.append(", ");
                str.append(key.name());
            }
            throw new IllegalArgumentException("Invalid strKeys, Please select one of list options: " + str.substring(2));
        }
        return "";
    }

    public static List<List<WebElement>> getListDocs() {
        WebDriver driver = WebDriverUtilities.getDriver();
        List<List<WebElement>> iframePaths = new ArrayList<>();
        try {
            By iframe = By.xpath(".//iframe");
            WebElement doc = (WebElement) ((JavascriptExecutor) driver).executeScript("return document;");
            iframePaths.add(new ArrayList<>());
            iframePaths.get(0).add(doc);
            for (int currIdx = 0; currIdx < iframePaths.size(); currIdx++) {
                try {
                    // Open iframe
                    switchFrame(iframePaths.get(currIdx));
                    // Search iframe and add to iframePaths
                    List<WebElement> elems = driver.findElements(iframe);
                    for (int idx = 0; idx < elems.size(); idx++) {
                        try {
                            WebElement elem = elems.get(idx);
                            if (!elem.isDisplayed())
                                continue;
                            List<WebElement> iframePath = new ArrayList<>();
                            iframePath.addAll(iframePaths.get(currIdx));
                            iframePath.add(elem);
                            iframePaths.add(iframePath);
                        } catch (StaleElementReferenceException e) {};
                    }
                } catch (StaleElementReferenceException e) {};
            }
        } finally {
            driver.switchTo().defaultContent();
        }
        System.out.println("find " + iframePaths.size() + " iframe");
        return iframePaths;
    }


    public static void switchFrame(List<WebElement> frames) {
        WebDriver driver = WebDriverUtilities.getDriver();
        for (int i = 0; i < frames.size(); i++) {
            if (i == 0) {
                driver.switchTo().defaultContent();
                continue;
            }
            WebElement elem = frames.get(i);
            driver.switchTo().frame(elem);
        }
    }

    enum NextActions {
        CONTINUE,STOP
    }

    enum LoopResult {
        LOOPALL, INTERRUPTED
    }

    interface LoopIframe {
        /**
        * @return true - continute loop iframe, false - stop loop iframe
        */
        NextActions eachIFrame() throws Exception;
        /**
        * @return true - loop through all iframe, false
        */
        default LoopResult loop() throws Exception {
            try{
                for (List<WebElement> docPath : getListDocs()) {
                    boolean isError = false; 
                    try {
                        switchFrame(docPath);
                    } catch (StaleElementReferenceException e) {
                        isError = true;
                    }
                    if(!isError && eachIFrame() == NextActions.STOP) return LoopResult.INTERRUPTED;
                }
                return LoopResult.LOOPALL;
            } finally {
                WebDriverUtilities.getDriver().switchTo().defaultContent();
            }
        }
    }

    interface FirstElementAction {
        void apply(WebElement e) throws Exception;

        default void perform(UIObject uiObject, Integer timeout) throws Exception {
            verifyElementPresent(uiObject, timeout);
            WebDriver driver = WebDriverUtilities.getDriver();
            LoopIframe a = () -> {
                WebElement elem = null;
                try {
                    elem = uiObject.convertToWebElement(driver);
                } catch (Exception e) {}
                if (elem != null) {
                    apply(elem);
                    return NextActions.STOP;
                }
                return NextActions.CONTINUE;
            };
            if(a.loop() == LoopResult.LOOPALL) throw new Exception("Element not present");
        }
    }

    interface AllElementAction {
        void apply(WebElement e) throws Exception;

        default void perform(UIObject uiObject, Integer timeout) throws Exception {
            verifyElementPresent(uiObject, timeout);
            WebDriver driver = WebDriverUtilities.getDriver();
            LoopIframe a = () -> {
                WebElement elem = null;
                try {
                    elem = uiObject.convertToWebElement(driver);
                } catch (Exception e) {}
                if (elem != null) {
                    apply(elem);
                }
                return NextActions.CONTINUE;
            };
            if(a.loop() == LoopResult.LOOPALL) throw new Exception("Element not present");
        }
    }

    public static void clearText(WebElement elem) {
        String script = "var setNativeValue = async (element, value) => {" + "    let lastValue = element.value;"
                + "    element.value = value;"
                + "    let event = new Event(\"input\", { target: element, bubbles: true });"
                + "    event.simulated = true;" + "    let tracker = element._valueTracker;" + "    if (tracker) {"
                + "        tracker.setValue(lastValue);" + "    };" + "    element.dispatchEvent(event);" + "};"
                + "setNativeValue(arguments[0], '')";
        ((JavascriptExecutor) WebDriverUtilities.getDriver()).executeScript(script, elem);
    }

    public static void clearText(UIObject uiObject, Integer timeout) throws Exception {
        FirstElementAction e = (WebElement elem) -> {
            clearText(elem);
        };
        e.perform(uiObject, timeout);
    }

    public static void setText(UIObject uiObject, Integer timeout, String text) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            clearText(elem);
            elem.sendKeys(text);
        };
        action.perform(uiObject, timeout);
    }

    public static void click(UIObject uiObject, Integer timeout) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            scrollToElement(elem);
            elem.click();
        };
        action.perform(uiObject, timeout);
    }

    public static void clickWithJavascript(UIObject uiObject, Integer timeout) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            scrollToElement(elem);
            ((JavascriptExecutor) WebDriverUtilities.getDriver()).executeScript("arguments[0].click()", elem);
        };
        action.perform(uiObject, timeout);
    }

    public static void doubleClick(UIObject uiObject, Integer timeout) throws Exception {
        WebDriver driver = WebDriverUtilities.getDriver();
        FirstElementAction action = (WebElement elem) -> {
            scrollToElement(elem);
            Actions act = new Actions(driver);
            act.doubleClick(elem);
        };
        action.perform(uiObject, timeout);
    }

    public static void rightClick(UIObject uiObject, Integer timeout) throws Exception {
        WebDriver driver = WebDriverUtilities.getDriver();
        FirstElementAction action = (WebElement elem) -> {
            scrollToElement(elem);
            Actions act = new Actions(driver);
            act.contextClick(elem).perform();
        };
        action.perform(uiObject, timeout);
    }

    public static void mouseOver(UIObject uiObject, Integer timeout) throws Exception {
        WebDriver driver = WebDriverUtilities.getDriver();
        FirstElementAction action = (WebElement elem) -> {
            Actions act = new Actions(driver);
            act.moveToElement(elem).perform();
        };
        action.perform(uiObject, timeout);
    }

    public static void scrollToElement(UIObject uiObject, Integer timeout) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            scrollToElement(elem);
        };
        action.perform(uiObject, timeout);
    }

    public static void scrollToElement(WebElement elem) {
        ((JavascriptExecutor) WebDriverUtilities.getDriver()).executeScript("arguments[0].scrollIntoView();", elem);
    }

    public static String getCSSAttribute(UIObject uiObject, String cssAttributeName, Integer timeout) throws Exception {
        List<String> data = new ArrayList<>();
        FirstElementAction action = (WebElement elem) -> {
            data.add(elem.getCssValue(cssAttributeName));
        };
        action.perform(uiObject, timeout);
        return data.get(0);
    }

    public static void setCSSAttribute(UIObject uiObject, String cssAttributeName, String newCssAttributeValue, Integer timeout) throws Exception {
        WebDriver driver = WebDriverUtilities.getDriver();
        FirstElementAction action = (WebElement elem) -> {
            ((JavascriptExecutor) driver).executeScript("arguments[0].obj.style['arguments[1]'] = 'arguments[2]';", elem, cssAttributeName, newCssAttributeValue);
        };
        action.perform(uiObject, timeout);
    }

    public static void sendSpecialKeyboardCharacter(UIObject uiObject, String strKeys, Integer timeout) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            try {
                elem.sendKeys(Keys.valueOf(strKeys));
            } catch (IllegalArgumentException e){
                Keys[] keys = Keys.values();
                StringBuilder str = new StringBuilder("");
                for(Keys key: keys) {
                    str.append(", ");
                    str.append(key.name());
                }
                throw new IllegalArgumentException("Invalid strKeys, Please select one of list options: " + str.substring(2));
            }
        };
        action.perform(uiObject, timeout);
    }

    public static void selectOptionByIndex(UIObject uiObject, Integer timeout, Integer index) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            objSelect.selectByIndex(index);
        };
        action.perform(uiObject, timeout);
    }

    public static void verifyOptionSelectedByIndex(UIObject uiObject, Integer timeout, Integer index) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            int actualIdx = objSelect.getOptions().indexOf(objSelect.getFirstSelectedOption());
            if (actualIdx != index) {
                throw new Exception(String.format("Selected Option has index is %d, but expected is %d", actualIdx, index));
            }
        };
        action.perform(uiObject, timeout);
    }

    public static void deselectOptionByIndex(UIObject uiObject, Integer timeout, Integer index) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            objSelect.deselectByIndex(index);
        };
        action.perform(uiObject, timeout);
    }

    public static void verifyOptionDeselectedByIndex(UIObject uiObject, Integer timeout, Integer index) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            List<WebElement> elems = objSelect.getOptions();
            if(elems.size() <= index) throw new Exception(String.format("the number of options are %d, less than index param %d", elems.size(), index));
            if (!elems.get(index).isSelected()) {
                throw new Exception(String.format("Option with index %d was selected", index));
            }
        };
        action.perform(uiObject, timeout);
    }

    public static void selectOptionByValue(UIObject uiObject, Integer timeout, String value) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            objSelect.selectByValue(value);
        };
        action.perform(uiObject, timeout);
    }

    public static void verifyOptionSelectedByValue(UIObject uiObject, Integer timeout, String value) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            String actualValue = objSelect.getFirstSelectedOption().getAttribute("value");
            if (!actualValue.equals(value)) {
                throw new Exception(String.format("selected Option has value is '%s', but expected is '%s'", actualValue, value));
            }
        };
        action.perform(uiObject, timeout);
    }

    public static void deselectOptionByValue(UIObject uiObject, Integer timeout, String value) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            objSelect.deselectByValue(value);
        };
        action.perform(uiObject, timeout);
    }

    public static void verifyOptionDeselectedByValue(UIObject uiObject, Integer timeout, String value) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            String actualValue = objSelect.getFirstSelectedOption().getAttribute("value");
            if (!actualValue.equals(value)) {
                throw new Exception(String.format("selected Option has value is '%s', but expected is '%s'", actualValue, value));
            }
        };
        action.perform(uiObject, timeout);
    }

    public static void selectOptionByLabel(UIObject uiObject, Integer timeout, String label) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            objSelect.selectByVisibleText(label);
        };
        action.perform(uiObject, timeout);
    }

    public static void verifyOptionSelectedByLabel(UIObject uiObject, Integer timeout, String label) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            String text = objSelect.getFirstSelectedOption().getText();
            if (!text.equals(label)) {
                throw new Exception(String.format("Selected Option has label is '%s', but expected is '%s'", text, label));
            }
        };
        action.perform(uiObject, timeout);
    }

    public static void deselectOptionByLabel(UIObject uiObject, Integer timeout, String label) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            objSelect.deselectByVisibleText(label);
        };
        action.perform(uiObject, timeout);
    }

    public static void verifyOptionDeselectedByLabel(UIObject uiObject, Integer timeout, String label) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            String text = objSelect.getFirstSelectedOption().getText();
            if (text.equals(label)) {
                throw new Exception(String.format("Option has label is '%s' was selected", label));
            }
        };
        action.perform(uiObject, timeout);
    }

    public static Integer getNumberOfSelectedOption(UIObject uiObject, Integer timeout) throws Exception {
        List<Integer> result = new ArrayList<>();
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            result.add(objSelect.getAllSelectedOptions().size());
        };
        action.perform(uiObject, timeout);
        return result.get(0);
    }

    public static Integer getNumberOfDeselectedOption(UIObject uiObject, Integer timeout) throws Exception {
        List<Integer> result = new ArrayList<>();
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            result.add(objSelect.getOptions().size() - objSelect.getAllSelectedOptions().size());
        };
        action.perform(uiObject, timeout);
        return result.get(0);
    }

    public static Integer getNumberOfOption(UIObject uiObject, Integer timeout) throws Exception {
        List<Integer> result = new ArrayList<>();
        FirstElementAction action = (WebElement elem) -> {
            Select objSelect = new Select(elem);
            result.add(objSelect.getOptions().size());
        };
        action.perform(uiObject, timeout);
        return result.get(0);
    }

    public static Integer getNunberOfLocator(UIObject uiObject) throws Exception {
        WebDriver driver = WebDriverUtilities.getDriver();
        List<WebElement> elems = new ArrayList<>();
        AllElementAction actions = (WebElement elem) -> {
            elems.addAll(uiObject.convertToWebElementList(driver));
        };
        actions.perform(uiObject, 1);
        return elems.size();
    }

    public static TestDataObject getTextFromMultipleElements(UIObject uiObject) throws Exception {
        List<String> data = new ArrayList<>();
        AllElementAction actions = (WebElement elem) -> {
            TestDataObject table = elemKwd.getTextFromMultipleElements(uiObject);
            for (String[] text : table.getDataTable()) {
                data.add(text[0]);
            }
        };
        actions.perform(uiObject, 1);
        String[] tableColumn = new String[] { "Text" };
        String[][] tableData = new String[data.size()][1];
        for (int idx = 0; idx < data.size(); idx++) {
            tableData[idx] = new String[] { data.get(idx) };
        }
        return new TestDataObject(tableColumn, tableData);
    }

    public static String getText(UIObject uiObject, Integer timeout) throws Exception {
        List<String> text = new ArrayList<>();
        FirstElementAction action = (WebElement elem) -> {
            text.add(elem.getText());
        };
        action.perform(uiObject, timeout);
        return text.get(0);
    }

    public static void submit(UIObject uiObject, Integer timeout) throws Exception {
        FirstElementAction action = (WebElement elem) -> {
            elem.submit();
        };
        action.perform(uiObject, timeout);
    }

    public static String executeJavaScript(String script) {
        WebDriverUtilities.getDriver().switchTo().defaultContent();
        Object obj = ((JavascriptExecutor) WebDriverUtilities.getDriver()).executeScript(script);
        if (obj == null)
            return "";
        return obj.toString();
    }

    public static void changeWindowSize(Integer width, Integer height) {
        WebDriver driver = WebDriverUtilities.getDriver();
        if (width == 0 || height == 0)
            driver.manage().window().maximize();
        driver.manage().window().setSize(new Dimension(width, height));
    }

    public static void verifyElementVisible(UIObject uiObject, Integer timeout) throws Exception {
        timeout = timeout * 1000;
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < timeout) {
            List<Boolean> visible = new ArrayList<>();
            FirstElementAction action = (WebElement elem) -> {
                visible.add(0, elem.isDisplayed());
            };
            action.perform(uiObject, timeout);
            if (!visible.isEmpty() && visible.get(0)) return;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {}
        }
        throw new Exception("Element is not visible");
    }

    public static void verifyElementNotVisible(UIObject uiObject, Integer timeout) throws Exception {
        boolean result = waitForElementNotVisible(uiObject, timeout);
        if (!result)
            throw new Exception("Element is still visible");
    }

    public static Boolean waitForElementNotVisible(UIObject uiObject, Integer timeout) throws Exception {
        timeout = timeout * 1000;
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < timeout) {
            List<Boolean> visible = new ArrayList<>();
            FirstElementAction action = (WebElement elem) -> {
                visible.add(0, elem.isDisplayed());
            };
            action.perform(uiObject, timeout);
            if (visible.isEmpty() || !visible.get(0)) return true;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {}
        }
        return false;
    }

    public static void verifyElementAttributeValue(UIObject uiObject, Integer timeout, String attributeName, String expectValue) throws Exception {
        String actualValue = getElementAttribute(uiObject, timeout, attributeName);
        if (!actualValue.equals(expectValue)) throw new Exception("Attribute " + attributeName + "has value is '" + actualValue + "'");
    }

    public static String getElementAttribute(UIObject uiObject, Integer timeout, String attributeName) throws Exception {
        verifyElementPresent(uiObject, timeout);
        List<String> text = new ArrayList<>();
        FirstElementAction action = (WebElement elem) -> {
            text.add(elem.getAttribute(attributeName));
        };
        action.perform(uiObject, timeout);
        return text.get(0);
    }

    public static void setAttribute(UIObject uiObject, String attributeName, String attributeValue, Integer timeout) throws Exception {
        WebDriver driver = WebDriverUtilities.getDriver();
        verifyElementPresent(uiObject, timeout);
        FirstElementAction action = (WebElement elem) -> {
            ((JavascriptExecutor) driver).executeScript("arguments[0].setAttribute(arguments[1],arguments[2]);", elem, attributeName, attributeValue);
        };
        action.perform(uiObject, timeout);
    }

    public static void verifyElementPresent(UIObject uiObject, Integer timeout) throws Exception {
        timeout = timeout * 1000;
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < timeout) {
            List<WebElement> elems = new ArrayList<>();
            FirstElementAction action = (WebElement elem) -> {
                elems.add(elem);
            };
            action.perform(uiObject, timeout);
            if(!elems.isEmpty()) return;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {}
        }
    }

    @SuppressWarnings("unchecked")
    public static String getConsoleBrowser() {
        WebDriver driver = WebDriverUtilities.getDriver();
        driver.switchTo().defaultContent();
        try {
            LogEntries logs = driver.manage().logs().get(LogType.BROWSER);
            List<LogEntry> listLog = logs.getAll();
            StringBuilder strLog = new StringBuilder();
            for (LogEntry log : listLog) {
                strLog.append(log.getLevel());
                strLog.append(" ");
                LocalDateTime time = Instant.ofEpochMilli(log.getTimestamp()).atZone(ZoneId.systemDefault())
                        .toLocalDateTime();
                strLog.append(time.toString());
                strLog.append(": ");
                strLog.append(log.getMessage());
                strLog.append("\n");
            }
            try {
                ArrayList<String> res = (ArrayList<String>) ((JavascriptExecutor) driver)
                        .executeScript("return window.logs;");
                if (res != null) {
                    for (String msg : res) {
                        strLog.append(msg);
                        strLog.append("\n");
                    }
                }
            } catch (Exception e) {}
            return strLog.toString();
        } catch (Exception e) {
            return "";
        }
    }
}