package util;

import java.nio.file.Path;
import java.nio.file.Paths;

public class FileSystem {
    public String getAbsolutePath(String relPath) {
        String projPath = System.getProperty("project.path");
        Path path = Paths.get(projPath, relPath);
        return path.toString();
    }
}