package platform;

import com.fpt.ivs.at.core.utilities.WebDriverUtilities;

import org.openqa.selenium.Dimension;

public class WebKeyword {
    public String executeScript(String script) {   
        Object obj = WebDriverUtilities.getDriver().executeScript(script);
        return obj == null ? "" : obj.toString();
    }

    public void changeWindowSize(Integer width, Integer height) {    //Start declare main test method, do not modify directly
        WebDriverUtilities.getDriver().manage().window().setSize(new Dimension(width, height));;
    }
}